(function () {
  'use strict';

  angular.module('yodaTranslate')
    .provider('yodaTranslate', translate);

  function translate () {
    var _config = null;

    this.setConfig = function setConfig (config) {
      _config = angular.extend((_config || {}), config);
    };

    this.$get = ['$http', '$q','$rootScope', $get];

    function $get ($http, $q, $rootScope) {
      var _loading = $q.defer(),
        _data = null,
        _lang = null;

      return function () {
        function fn (key) {
          return _loading.promise.then(function () {
            return translate(key);
          });
        }

        fn.run = run;
        fn.setLanguage = setLanguage;
        fn.now = translate;
        fn.done = _loading.promise;
        fn.currentLanguage = _lang;

        return fn;
      }();

      function run () {
        _requestTranslation();

        return _loading.promise;
      }
      
      function setLanguage (lang) {
        _lang = lang;
        return this;
      }

      function setTranslation (data) {
        _data = angular.extend((_data || {}), data);
      }

      function translate (key) {
        return (_data || {})[key] || key;
      }
     
      function _requestTranslation(){
        $http.get(_compileUrl(_lang))
        .then(function (res) {
          setTranslation(res.data);
          _loading.resolve();
        })
        .catch(function(error){
            
           $http.get(_compileUrl(_config.preferredLanguage))
           .then(function (res) {
             setTranslation(res.data);
             _loading.resolve();
            })
            .catch(function(error){
              _loading.reject();
            });
        });
      }
      
      function _compileUrl (lang) {
        return _config.url.replace(_config.pattern || '{lang}', lang);
      }
      
      _loading.promise.then(function(){
         $rootScope.$emit('yodaTranslate:done'); 
      });
      
    }
  }
}());
