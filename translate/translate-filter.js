(function(){
	'use strict';
	
	angular.module('yodaTranslate')
		.filter('yodaTranslate', ['yodaTranslate', function(yodaTranslate){
            var fn = angular.identity;
            
            yodaTranslate.done.then(function(){
               fn = function real(key){
                   return yodaTranslate.now(key);
               };
            });
           
            function filter(key){
				return fn(key);
			}

            filter.$stateful = true;
            
            return filter;
		}]);
}());
